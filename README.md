# Memory Game Project

## Table of Contents

* [Instructions](#instructions)



## Instructions
Click on a card
Keep revealing cards and working your memory to remember each unveiled card.
Match cards properly with less moves and in faster time

## How was built the Memory Game
I manipulated the DOM with JS, altered part of the HTML and also styled the game

created a deck of cards that shuffles when game is refreshed
created a counter to count the number of moves made by player and timer to know the duration of a play
added effects to cards when they match and are unmatched
create a pop-up modal when player wins game