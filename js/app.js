const deck = document.getElementById("deck");
let openCards = [];
let moveCounter = 0;
let guessedCards = 0;
let numberOfAttemps = 0;
let openCardsCounter = 0;
let second = 0,
    minute = 0,
    hour = 0;
let start = new Date;
let interval;
let finalTime = 0,
    starRating = 0,
    finalMoves = 0,
    isFirstClick = true;
let cardDiamond = 'fa-diamond';
let cardPaper = 'fa-paper-plane-o';
let cardAnchor = 'fa-anchor';
let cardBolt = 'fa-bolt';
let cardCube = 'fa-cube';
let cardLeaf = 'fa-leaf';
let cardBomb = 'fa-bomb';
let cardBicycle = 'fa-bicycle';

document.addEventListener("DOMContentLoaded", function() {
    loadGame();
    document.getElementById("restart").addEventListener("click", function(e) {
        loadGame();
    })
    document.getElementById("button").addEventListener("click", function(e) {
        loadGame();
        document.getElementById("success-message").style.display = "none";
    })
});

// Create a list that holds all of your cards
var listOfCards = [cardDiamond, cardDiamond, cardPaper, cardPaper, cardAnchor, cardAnchor, cardBolt, cardBolt, cardCube, cardCube, cardLeaf, cardLeaf, cardBomb, cardBomb, cardBicycle, cardBicycle];

function loadGame() {
    listOfCards = shuffle(listOfCards);
    openCards = [];
    openCardsCounter=0;
    moveCounter = 0;
    numberOfAttemps = 0;
    isFirstClick = true;
    second = 0;
    minute = 0;
    hour = 0;
    deck.innerHTML = '';
    for (let index = 0; index < listOfCards.length; index++) {
        var node = document.createElement("LI");
        node.classList.add("card");
        node.id = 'card-' + index;
        var iconNode = document.createElement("I");
        iconNode.classList.add("fa");
        iconNode.classList.add(listOfCards[index]);
        node.appendChild(iconNode);
        deck.appendChild(node);
        node.addEventListener("click", function(e) {
            if (openCardsCounter != 2) {
                openCard(e);
            }
            if (isFirstClick) {
                isFirstClick = false;
                startTimer();
            }
        });

    }
    document.getElementById("timer").innerHTML = "0min 0sec";
    document.getElementById("moves").innerHTML = '0';
    clearInterval(interval);
    resumeStart();
}
// Shuffle function from http://stackoverflow.com/a/2450976
function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function resumeStart() {
    var stars = document.getElementsByClassName("rating");
    for (var index = 0; index < stars.length; index++) {
        stars[index].classList.add("fa-star");
        stars[index].classList.remove("fa-star-o");

    }
}


function openCard(e) {
    if (e.target.className.indexOf('open') == -1 && e.target.nodeName != 'I') {
        e.target.classList.add("open");
        e.target.classList.add("show");
        openCardsCounter++;
        checkIfCardsMatches(e);
    }
}

function checkIfCardsMatches(e) {

    if (openCards.length == 1) {
        numberOfAttemps++;
        manageRating();
        if (openCards[0].childNodes[0].className == e.target.childNodes[0].className && openCards[0].id != e.target.id) {
            e.target.classList.add("match");
            openCards[0].classList.add("match");
            guessedCards++;
            checkIfGameIsOver();
            manageCounter();
            openCards = [];
            openCardsCounter = 0;
            return;
        } else {
            setTimeout(function() {
                closeCard(e.target, openCards[0]);
                openCards = [];
                manageCounter();
                return;
            }, 1000);
        }

    }
    openCards.push(e.target);
}


function closeCard(firstCard, secondCard) {
    firstCard.classList.remove("open");
    firstCard.classList.remove("show");
    secondCard.classList.remove("open");
    secondCard.classList.remove("show")
    openCardsCounter -= 2;

}

function manageCounter() {
    moveCounter++;
    document.getElementById("moves").innerHTML = moveCounter;
}

function checkIfGameIsOver() {
    if (guessedCards == 8) {
        document.getElementById("success-message").style.display = "inline";
        finalTime = timer.innerHTML;
        document.getElementById("finalMove").innerHTML = moveCounter;
        document.getElementById("starRating").innerHTML = document.getElementById("stars").innerHTML;
        document.getElementById("totalTime").innerHTML = finalTime;
    }
}

function manageRating() {
    if (numberOfAttemps > 10 && numberOfAttemps <= 15) {
        document.getElementsByClassName("rating")[2].classList.remove("fa-star");
        document.getElementsByClassName("rating")[2].classList.add("fa-star-o");
    } else if (numberOfAttemps > 15) {
        document.getElementsByClassName("rating")[1].classList.remove("fa-star");
        document.getElementsByClassName("rating")[1].classList.add("fa-star-o");
    }
}

/*
setInterval(function() {
    document.getElementById("timer").innerHTML=((new Date - start) / 1000 + " Seconds");
});
*/
// @description game timer

function startTimer() {
    var timer = document.querySelector(".timer");
    interval = setInterval(function() {
        if (!isFirstClick) {
            timer.innerHTML = minute + "min " + second + "sec";
            second++;
            if (second == 60) {
                minute++;
                second = 0;
            }
            if (minute == 60) {
                hour++;
                minute = 0;
            }
        }
    }, 1000);
}

/*
 * Display the cards on the page
 *   - shuffle the list of cards using the provided "shuffle" method below
 *   - loop through each card and create its HTML
 *   - add each card's HTML to the page
 */
/*
 * set up the event listener for a card. If a card is clicked:
 *  - display the card's symbol (put this functionality in another function that you call from this one)
 *  - add the card to a *list* of "open" cards (put this functionality in another function that you call from this one)
 *  - if the list already has another card, check to see if the two cards match
 *    + if the cards do match, lock the cards in the open position (put this functionality in another function that you call from this one)
 *    + if the cards do not match, remove the cards from the list and hide the card's symbol (put this functionality in another function that you call from this one)
 *    + increment the move counter and display it on the page (put this functionality in another function that you call from this one)
 *    + if all cards have matched, display a message with the final score (put this functionality in another function that you call from this one)
 */
